<?php
try {
    $sql = new PDO ( 'mysql:dbname=' . getenv('MYSQLCONNSTR_DATABASE') . '; host=' . getenv('MYSQLCONNSTR_HOST') . '; port=3306; charset=utf8', getenv('MYSQLCONNSTR_USER'), getenv('MYSQLCONNSTR_PASSWORD') );
    print 'MySQLへの接続に成功しました。';
} catch ( PDOException $e ) {
    print "接続エラー:{$e->getMessage()}";
}
$sql = null;
?>